require 'rubygems'
require 'UAClient'
require 'hashie'
require 'redis'
require 'json'
require 'thread'
require 'stringex'
require 'digest/md5'
require 'uri-find'

# Expiry time of seen URLs in seconds
$expiry_time = (3.5 * 86400).to_i

Thread.abort_on_exception = true
$semaphore = Mutex.new
$cache = { :subjects => {}, :urls => {}, :pending => Hash.new{|h,k|h[k]=[]}}

def make_url_key(url)
    return "__ua_key_" + Digest::MD5.hexdigest(url)
end

class TitleBot < UAClient
    attr_accessor :last_periodic
    attr_accessor :redis

    def initialize(*opts)
        super(*opts)
        @redis = Redis.new
    end

def hashify(x, opts={})
    y = Hashie::Mash.new
    z = opts[:list] || []

    x.children.each do |child|
        new_val = child.value

        if not child.children.empty? then # leaf node
            new_val = hashify(child, opts)
            new_val._value = child.value
        end

        if y[child.key].nil? then
            if z.include?(child.key) then
                y[child.key] = [new_val]
            else
                y[child.key] = new_val
            end
        else
#            puts "#{child.key} is a list"
            y[child.key] = [y[child.key], new_val].flatten
        end
#        puts "K #{child.key} / #{z.include?(child.key)} / #{z.inspect} -> #{y[child.key]}"
    end

    return y
end

# <reply="message_list"><message=2276419><date=1445895845/><fromid=3629/><toid=1/><text="I doubt there are many filesystems which cope with a drive being unplugged whilst being written to."/><threadid=2276150/><subject="fsck_hfs"/><fromname="rjp"/><toname="Gryn"/><replyto=2276418><read=1/><fromid=1/><fromname="Gryn"/></><replyto=2276150><fromid=3629/><fromname="rjp"/></><msgpos=115/></><searchtype=0/><folderid=1769978/><foldername="Apple"/><nummsgs=126/></>
# <reply="message_list"><message=2283621><date=1449580124/><fromid=3629/><text="Since I'm guessing I've forgotten how edits appear in the message list"/><threadid=2283621/><subject="TESTING"/><attachment=1><content-type="text/x-ua-annotation"/><text="THIS IS AN EDIT BADGER SPRAINTS OTTER WEASELS"/><fromid=3629/><date=1449580145/><fromname="rjp"/></><edit=1449580145/><fromname="rjp"/><msgpos=4/></><searchtype=0/><folderid=307401/><foldername="test1"/><nummsgs=4/></>
def reply_message_list(x, edf)
    m = hashify(x, :list => %w/replyto attachment/)
    puts m.inspect

    parent = nil

    # We have a `<replyto>` block
    if not m.message.replyto.nil? then
        parent = m.message.replyto.first._value
    end

    places_for_urls = [ m.message.text ]
    if not m.message.attachment.nil? then
        m.message.attachment.each do |i|
            places_for_urls << i.text
        end
    end

    puts "#{m.foldername}/#{m.message._value} #{m.message.fromname} => #{m.message.toname} p=#{parent} [#{m.message.text[0..39].gsub(/\n/,' ')}] #{places_for_urls.size}"
    $cache[:subjects][m.message._value] = m.message.subject

    if m.message.fromname == @user then
        return
    end

# URLs can be hard-wrapped over a break by `qUAck`. BECAUSE.
# To avoid this, we just remove all the breaks and hope that
# nothing new breaks.
    wrapped = {}
    unique_urls = {}

# Bit of a fudge but should work fine.
    search_text = places_for_urls.join(" ")

    simple_find(search_text, ['http', 'https']).each do |url|
        if url.size == 79 then
#            puts "HARD WRAPPED: #{url}"
            wrapped[url] = true
        else
            unique_urls[url] = true
        end
    end

# If we found any URLs that looked hard-wrapped, have another pass
    if wrapped.size > 0 then

# For every URL that looked hard-wrapped, remove the following `\n`
        wrapped.keys.each do |truncated|
           search_text.gsub!(/^#{truncated}\n/, truncated)
        end

# Look for URLs again.
        simple_find(search_text, ['http', 'https']).each do |url|
            unique_urls[url] = true
        end
    end
    urls_found = unique_urls.keys

        $semaphore.synchronize {
# It's safe to set this directly since we'll never see a message
# again and will never need to change this number until we've got
# a reply back from the metadata service
#             puts "Caching the size for #{m.message._value} = #{urls_found.size}"
            $cache[:urls][m.message._value] = urls_found.size
        }

        urls_found.each do |url|
            @redis.publish("urls-from-ua.json", { "URL" => url, "MessageID" => m.message._value }.to_json)
            puts "-> #{url} @#{m.message._value}"
        end
end

    def announce_message_add(x, edf, is_edit=false)
        # puts x.inspect
        y = Hashie::Mash.new
        x.children.each do |child|
            if child.children.empty? then # leaf node
                if y[child.key].nil? then
                    y[child.key] = child.value
                else
                    y[child.key] = [y[child.key], child.value].flatten
                end
            end
        end
        if is_edit == false then
            if y.subject.nil? then
                y.subject = "[keine subjecten]"
            end
            $cache[:subjects][y.messageid] = y.subject
# Programming/2278497 rjp =>  [This is a good read about Unicode in gen]
            puts "#{y.foldername}/#{y.messageid} #{y.fromname} => #{y.toname} [#{y.subject[0..39]}]"
        else
#             <announce="message_edit"><messageid=2283623/><folderid=307401/><foldername="test1"/><announcetime=1449580551/><marked=2/></>
            puts "#{y.foldername}/#{y.messageid} EDIT"
        end
        request('message_list', { "messageid": y.messageid })
    end

    # This is basically the same path through the code
    def announce_message_edit(x, edf)
        announce_message_add(x, edf)
    end

    def do_periodic()
        puts "+++ periodic called at #{Time.now()} / #{@last_periodic}"
    end

    def reply_to(messageID, body)
        puts "I would post to #{messageID} -> [#{body[0..25]}]"

# NB: HARDCODED TO POST IN `test2`
# (should be configurable and look it up when we boot the bot)
        subject = $cache[:subjects][messageID]
        if subject.nil? then
            throw "Cannot have an empty subject!"
        end
        request_body = { "replyid": messageID, "text": body, "folderid": 307405, "subject": subject }
        request('message_add', request_body)
        puts request_body.inspect
    end
end

$loginbot = TitleBot.new("ua2.org", 4040, {:shadow => 256 })

# Frob the twiddles every ~5 minutes
poller = Thread.new {
    $loginbot.do_periodic()
    sleep 300
}

bot = Thread.new {
    puts "Logging in as #{ARGV[0]}/#{ARGV[1]}"
    $loginbot.start(ARGV[0], ARGV[1])
}

poster = Thread.new {
    r_reader = Redis.new
    # Can't use other redis commands on a pubsub connection
    r_expiry = Redis.new

    r_reader.subscribe("url-metadata") do |on|
        on.message do |channel, message|
            data = Hashie::Mash.new( JSON.parse(message) )
            mID = data.MessageID
            do_reply = false
#            puts "Got a reply for #{mID}"

# In theory, there's no way we can get to this point without already
# having something set in `$cache[:urls]` for this message but we might
# as well synchronize to be on the safe side
            $semaphore.lock
                puts "Pending for #{mID} = #{$cache[:urls][mID]}"
                if $cache[:urls][mID] == 0 then
                    throw "Received data when we have none pending"
                end
                # Keep this metadata handy
#                puts "Storing the data for #{mID}"
                $cache[:pending][mID] << data
#                puts "Stored the data for #{mID}"
                # Countdown to posting
                $cache[:urls][mID] = $cache[:urls][mID] - 1
#                puts "Remaining for #{mID} = #{$cache[:urls][mID]}"
                if $cache[:urls][mID] == 0 then
                    do_reply = true
                end
            $semaphore.unlock

            if do_reply then
                # Only look at URLs with `text/*` content
                body = $cache[:pending][mID].select { |url|
                    url.ContentType =~ %r{text/}
                }.reject { |url|
                    r_expiry.exists(make_url_key(url.URL))
                }.map { |url|
                    r_expiry.setex(make_url_key(url.URL), $expiry_time, url.URL)

                    # `Stringex` makes a right mess of pound signs and doesn't provide
                    # a nice simple way to override this.  Since I can't be bothered forking
                    # and fixing right now, let's fudge the issue with a preconversion.
                    url.Description.gsub!(/\u00A3/, "GBP")
                    url.Title.gsub!(/\u00A3/, "GBP")

                    safe_title = Stringex::Unidecoder.decode(url.Title).gsub(/"/,'\\"')
                    safe_description = Stringex::Unidecoder.decode(url.Description).gsub(/"/,'\\"')
                    puts "#{safe_title}\n#{url.URL}\n\n#{safe_description}"
                    "#{safe_title}\n#{url.URL}\n\n#{safe_description}"
                }.join("\n----\n")
                if body.size > 0 then
                    puts body
                    $loginbot.reply_to(data.MessageID, body)
                end
            end
        end
    end
}

bypass = Thread.new {
    r_reader = Redis.new
    r_writer = Redis.new
    puts "SUBSCRIBING TO URL-FAKER"
    r_reader.subscribe("url-faker") do |on|
        on.message do |channel, message|
            data = Hashie::Mash.new( JSON.parse(message) )
            mID = data.MessageID
            urls_found = data.URLs
            puts "! FOUND #{mID} #{urls_found}"
    # It's safe to set this directly since we'll never see a message
    # again and will never need to change this number until we've got
    # a reply back from the metadata service
            puts "! Caching the size for #{mID} = #{urls_found.size}"
            $cache[:urls][mID] = urls_found.size

            urls_found.each do |url|
                puts "Sending out URL=#{url}"
                r_writer.publish("urls-from-ua.json", { "URL" => url, "MessageID" => mID }.to_json)
                puts "-> #{url}"
            end
        end
    end
}

forcer = Thread.new {
    r_reader = Redis.new
    r_reader.subscribe("message-forcer") do |on|
        on.message do |channel, message|
            puts "FORCED: #{message.to_i}"
            $loginbot.request('message_list', { "messageid": message.to_i })
        end
    end
}

forcer.join
poster.join
bot.join
poller.join
