$:.push('/home/rjp/git/ua/edf')
require 'EDF'
require 'log4r'
include Log4r

# TODO this needs to be configurable and optioned
# icky constant nastiness
FIONREAD=0x541B

require 'socket'

# class UA
class UAClient
	attr_reader :last_data, :users, :edf_on, :events
    attr_reader :user, :password, :agent, :status
    attr_reader :l4r
    attr_accessor :last_tick

    def sock_write(x)
#$stderr.puts "S #{x}"
        @socket.write x
    end

def open(host, port, timeout=10)
 addr = Socket.getaddrinfo(host, nil, :INET)
 sock = Socket.new(Socket.const_get(addr[0][0]), Socket::SOCK_STREAM, 0)

 begin
  sock.connect_nonblock(Socket.pack_sockaddr_in(port, addr[0][3]))
 rescue Errno::EINPROGRESS
  resp = IO.select([sock],nil, nil, timeout.to_i)
  begin
    sock.connect_nonblock(Socket.pack_sockaddr_in(port, addr[0][3]))
  rescue Errno::EISCONN
  end
 end
 sock
end

	def disconnect
		@edf_on = nil
		request('user_logout')
		@socket.close
	end

	def initialize(host="ua2.org", port=4040, meta={})
		@socket = open(host, port)
		@edf = EDF.new()
		@login = nil
		@user = nil
		@edf_on = nil
		@users = []
        @password = nil
        @agent = nil
        @status = nil
        @online = nil
        @last_tick = Time.now().to_i

# random metadata we like to carry around
		@meta = { 'client'=>'Ruby/EDF 0.01', 'protocol'=>'2.4-beta9' }
		@meta.update(meta)
		@meta['clientbase'] = @meta['client'].split(' ')[0]

        @l4r = Logger.new 'uaclient.log'
        f = FileOutputter.new 'file', :filename => '/data/rjp/logs/uaclient', :trunc => false
        f.formatter = PatternFormatter.new :pattern => '%l [%d] %80m'
        @l4r.outputters = f

	end

# login should be:
# send edf start message
# wait for edf event
# send user_login
# wait for ok, you're logged in

# bleh
	def login(username='', password='', agent=nil, status=nil)
        @user = username
        @password = password
        @agent = agent
        @status = status
		count = self.sock_write EDF.hash_to_string({'edf'=>'on'}) + "\n"
    end

def xbox(x)
# this is wrong, we should let the normal event handling occur here
		tree, raw = check_reply
        p raw
		if tree then
			if tree[0]["edf"] == 'on' then
				@edf_on = true
			else
				raise
			end
		end
		hash = @meta.dup.update({ 'name' => username, 'password' => password })
		unless agent.nil? then
			hash['status'] = 16
			hash['client'] = agent
		end
		unless status.nil? then
			hash['status'] = status
		end
		hash['statusmsg'] = 'Logging in'
		request("user_login", @meta.dup.update(hash))
		reply, raw = check_reply # catch ourselves logging in
	end

	def user_login(r)
	end

	def handle_user_list(r)
		r.children.each { |x|
			if x != nil then
				h = x.to_hash
				if h['name'] then
					h['id'] = x['user']
					@users[h['name'].downcase] = h
				end
			end
		}
	end

# create a user list
	def user_list
		@users = Hash.new
		request("user_list", {'searchtype' => 3} )
	end

# if a user has a 'timeoff' attribute, they're not logged on
# we shoULD REally use a 'user_list' request with 'searchtype' set
	def who_list
		return @users.collect { |x| x['timeoff'].nil? }
	end

	def user(username)
		request("user_list", { 'name' => username } )
		reply, raw = check_reply
		handle_user_list reply[0]
		return @users[username.downcase]
	end

	def busy(username)
		# should we check status=7 as well?
		uobj = self.user(username)
		return nil if uobj.nil?
		return user(username)['busymsg']
	end

	def userid(username)
		return user(username)['id']
	end

	def pageable(username)
		u = self.user(username)
		return nil if u.nil?
		print "#{username} exists, at least"
		return self.busy(username)
	end

	def page(username, text)
		@l4r.debug "paging #{username} with [#{text}]"
		uid = userid(username)
		request("user_contact", {
				'toid' => uid, 'contacttype' => 0,
				'override' => 0, 'text' => text })
#		raise reply.value if reply.value != 'user_contact'
	end

	def check_reply
		to_read = ["1", 2] # fake
		data = ""
        # if we've read something and select says we can read()...
		while to_read[0].length > 0 and a = select([@socket], nil, nil, 2) do
            to_read = @socket.recvfrom_nonblock(65535)
#$stderr.puts to_read.inspect
            if to_read[0].length > 0 then
    			data << to_read[0]
            end
		end

		if data.length > 0 then
#        $stderr.puts data.inspect
			@last_data = data
            @l4r.info(data)
			x = @edf.parse(data), data
            return x
		else
			return nil
		end
	end

	def request(type, hash=nil)
		if hash.nil? then
	   		a = %Q{<request="#{type}"/>}
		else
			request = EDF::hash_to_string(hash)
			a = %Q{<request="#{type}">#{request}</>}
		end
		@l4r.debug(a.inspect)
		self.sock_write(a)
	end

	def get(key)
		return @edf.get(key)
	end

	def handle_async(event, handler)
		@events[event] = handler
	end

	def handle_reply(event, handler)
		@events[event] = handler
	end

	def poll()
        now = Time.now().to_i
        if now - @last_tick > 320 then
        @l4r.info("! #{now} - #{@last_tick} > 320")
            @last_tick = now
            self.send('periodic')
        end
		async, raw = self.check_reply
		if async then
			async.each { |x|
				type = 'announce'
				event = x['announce']
				unless event then
					event = x['reply']
					type = 'reply'
				end
                unless event then
                    event = x.key
                    type = 'raw'
                end

				method = "#{type}_#{event}"
				@l4r.info( "====> #{method}")

				self.send(method, x, raw)
			}
		end
	end

# other is some sort of ancilliary method for extra work
	def start(user, pass, other=nil)
        self.login(user, pass)
		loop do
			self.poll()
			other.call() unless other.nil?
		end
	end

	def method_missing(*x)
#		puts "Unhandled #{x[0]}"
#        p x[1..-1]
	end

    def raw_edf(x, y)
        if x['edf'] == 'on' then
            @edf_on = true
        end

		hash = @meta.dup.update({ 'name' => @user, 'password' => @password })
		unless @agent.nil? then
			hash['status'] = 16
			hash['client'] = @agent
		end
		unless @status.nil? then
			hash['status'] = @status
		end
		hash['statusmsg'] = 'Logging in'
        if @meta[:shadow] then
            hash['status'] = hash['status'] | 256
        end
        hash['status'] = 256
		request("user_login", @meta.dup.update(hash))
    end

    def reply_user_login(x, raw)
        if x['name'] == @user then
            @online = true
            @l4r.debug "logged in, off we go"
        end
    end
end
