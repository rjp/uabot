# when we open a tag, open a new array of children
#	stack.push({name=>,value=>,children=>[]})
# when we close a tag, close the array of children
#	stack.pop()
# when we get a tag, add it to the current array of children
#	stack[-1]['children'].push(curtag)
class EDF
  attr_reader :tree
  #
  def initialize()
  end

	def EDF.hash_to_string(h)
		s = ''
		h.each { |k,v|
			if v.class == String then
				s << %Q{<#{k}="#{v}"/>}
			else
				s << %Q{<#{k}=#{v}/>}
			end
		}
		return s
	end

    class Node
        attr_accessor :key, :value
        def initialize(k, v)
            @key, @value = k, v
        end
    end

  class Tree
    attr_accessor :children, :key, :value
    def initialize()
		@children=[]
		@debug=1
    end

	def [](key)
#    $stderr.puts "E getChild(#{key})"
		t = getChild(key)
		return nil if t.nil?
		return t.value
	end

	def getChild(key)
		return self if @key == key
		return @children.find {|x| x.key == key }
	end

	def to_hash()
		h = Hash.new()
		@children.each { |x|
			h[x.key] = x.value
		}
		return h
	end

    def to_data()
        child_data = []
        self.children.each do |child|
            child_data.push [child.to_data]
        end
        return self.key, self.value, child_data
    end

    class Reply
    end

    #<announce="user_status"><userid=31/><username="Golgotha"/><gender=2/><status=1/><announcetime=1166785540/></>
    class Announce
    end
  end

  # return an EDF::Tree object
  def parse(data)
	# convert escaped quotes into something safe
	data.gsub!(%r{[\\]"}, '#22#')
#    $stderr.puts "E #{data}"
	# convert quoted ">" into something safe
	data.gsub!(%r{"(.*?)"}m) { |x|
		x.gsub('>', '#3e#')
	}
#    $stderr.puts "E #{data}"
	stack=[]
	roots=[]
    data.scan(%r{<(.*?)(/?)([^/]*?)>}m) { |x|
#        $stderr.puts x.inspect
		tag, close, after = x
		if tag=="" and close=="" then tag=after end
		if tag=="" and close=="" and after=="" then close=1 end
		element, value = tag.split('=', 2)
#            puts "#{element}, #{element==''}"
		if not element.nil? and element.length > 0 then
#            puts "#{tag}, #{element}, #{value}, #{close}"
# $stderr.puts "T " << ('.' * stack.size) << " e=#{element} v=#{value}"
			tmp = EDF::Tree.new
			value ||= %q{"1"}
			puts "--> #{element} == #{value}" if @debug
			if value =~ /^"/ then
				value.gsub!('#22#', '"')
				value.gsub!('#3e#', '>')
				value.gsub!(%r![\\]{2}!,'\\')
				tmp.value = value.gsub(/^"/,'').gsub(/"$/,'')
#                $stderr.puts "v=#{value} tv=#{tmp.value}"
			else
				tmp.value = value.to_i
			end
			tmp.key = element

			if stack.size > 0 then
                stack[-1].children.push(tmp)
#                $stderr.puts "+ adding #{element}=#{tmp.value} to #{stack.size-1} "
            end
			if stack.size == 0 then
				roots.push(tmp)
			end
			stack.push(tmp)
		end

		# once we pop, the stack size drops
		if close==1 or close=='/' then
#            $stderr.puts "= #{stack.size}"
            stack.pop
#            $stderr.puts "- #{stack.size}"
        end
    }

# this is a prototype callback mechanism.  Ish.
#t.children.each { |x|
#	case x.key
#		when 'reply'
#			puts "REPLY: #{x}"
#		when 'announce'
#			puts "ANNOUNCE: #{x}"
#	end
#}

    new_roots = []
    roots.each do |root|
        new_roots.push root
    end

	return new_roots

  end
end

def treealise(tree, depth=0)
    indent = " " * depth
    puts indent + tree.key + "=" + tree.value.to_s[0..24]
    tree.children.each do |child|
        treealise(child, depth+1)
    end
end

if __FILE__ == $0 then
		a = EDF.new()
		f = ARGV[0] || "test.edf"
		data = IO.readlines(f).join('')
		b = a.parse('<edf="on"><time=12345/></>') # data)
        b = a.parse(data)
        puts b.inspect

        t = b[0]

        q = treealise(t)

		if f == 'big.edf' then
			b.children.each {|x|
				puts "Username #{x['name']} = #{x['user']}" if x['user']
			}
			exit
		end

		if t.children.size > 1 then # FLID ABOUT LIKE A CHICKEN
			p b
		end
#		puts b.getChild("reply").getChild("banner").value

		c = EDF::hash_to_string({'moo'=>1, 'cow'=>'fish'})
#		puts c
end
